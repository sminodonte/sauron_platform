#!/bin/bash

# DELETE NODE FROM NETWORK
eval "$(grep ^GATE_NODE_DEVICE_UUID= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_TOKEN= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_INIT_UUID= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_INIT_TOKEN= do_not_touch_file)"
eval "$(grep ^MIDDLENODE_HOSTNAME= do_not_touch_file)"
eval "$(grep ^MIDDLENODE_HTTP_PORT= do_not_touch_file)"

curl -X DELETE \
  http://"${MIDDLENODE_HOSTNAME}":"${MIDDLENODE_HTTP_PORT}"/devices/$GATE_NODE_DEVICE_UUID \
  -H "meshblu_auth_token: ${GATE_NODE_DEVICE_TOKEN//[$'\t\r\n ']}" \
  -H "meshblu_auth_uuid: ${GATE_NODE_DEVICE_UUID//[$'\t\r\n ']}
"
curl -X DELETE \
  http://"${MIDDLENODE_HOSTNAME}":"${MIDDLENODE_HTTP_PORT}"/devices/$GATE_NODE_DEVICE_INIT_UUID \
  -H "meshblu_auth_token: ${GATE_NODE_DEVICE_INIT_TOKEN//[$'\t\r\n ']}" \
  -H "meshblu_auth_uuid: ${GATE_NODE_DEVICE_INIT_UUID//[$'\t\r\n ']}
"
sleep 4s
# STOP AND DELETE gate-node_platform CONTAINER 
docker-compose down
sleep 2s

# DELETE DATABASE AND LOGS
sudo rm -rf data
sudo rm -rf logs

# DELETE ALL IMAGES OF gate-node platform
docker rmi $GATENODE_CORE_IMAGE
docker rmi $GATENODE_GUI_IMAGE
